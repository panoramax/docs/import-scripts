#! /usr/bin/env python3
"""
    Tool to convert Kandao .kvar files into JSON format

    written by cquest, shared under WTFPL
"""

import os, sys, argparse, json, re
from datetime import datetime
from struct import unpack

parser = argparse.ArgumentParser(
    prog='kvar2json',
    description='Tool to convert Kandao .kvar files into JSON format',
    epilog='written by cquest, shared under WTFPL')
parser.add_argument('kvar_file', nargs=1, help='.kvar input file to process')
args = parser.parse_args()

print('{ "kvar_file": "%s",' % os.path.basename(args.kvar_file[0]))

def bytes2str(b):
    return b.decode().strip(chr(0))

START = re.sub(r'^.*(\d{4})(\d{2})(\d{2})_(\d{2})(\d{2})(\d{2}).*$',r'\1-\2-\3T\4:\5:\6',args.kvar_file[0])
EPOCH = datetime.strptime(START, "%Y-%m-%dT%H:%M:%S").timestamp()

print(' "start": "%s",' % START)
print(' "epoch": "%s",' % EPOCH)
pts_unit = 1000

with open(args.kvar_file[0], 'rb') as kvar:
    dummy, sign, sections = unpack('<4s4sI',kvar.read(12))
    if bytes2str(sign) != 'kvar':
        # no header in file ?
        kvar.seek(0)
        sections, = unpack('<I',kvar.read(4))
    for section in range(sections):
        title, data_type, data_count = unpack('<32s8sI',kvar.read(44))
        title = bytes2str(title)
        if title == 'TOTAL_FRAME':
            frames, = unpack('<I',kvar.read(4))
            print('  "frames": %s,' % frames)
        elif title == 'TOTAL_TIME_MS':
            time_ms, = unpack('<I',kvar.read(4))
            print('  "duration": %s,' % (time_ms/1000))
        elif title == 'LENS':
            lens, = unpack('%ss' % data_count ,kvar.read(data_count))
            print('  "lens": "%s",' % bytes2str(lens))
        elif title == 'IMU':
            print('  "imu": [')
            for idx in range(int(data_count/20)):
                pts, giro_x, giro_y, gyro_z, acc_x, acc_y, acc_z = unpack('<Qhhhhhh', kvar.read(20))
                #print(pts/1000000, giro_x, giro_y, gyro_z, acc_x, acc_y, acc_z)
                print('{"pts": %s, "giro_x": %s, "giro_y": %s, "gyro_z": %s, "acc_x": %s, "acc_y": %s, "acc_z": %s }%s' % (pts/1000000, giro_x, giro_y, gyro_z, acc_x, acc_y, acc_z, ',' if idx<int(data_count/20)-1 else ''))
            print('  ],')
        elif title == 'GPS':
            kvar.read(4)
            print('  "gps": [')
            for idx in range(int(data_count/28)):
                pts, lat, lon, alti = unpack('<Lddd', kvar.read(28))
                print('{"pts": %s, "lat": %s, "lon": %s, "alt": %s }%s' % (pts/1000, round(lat,7), round(lon,7), round(alti,1), ',' if idx<int(data_count/28)-1 else ''))
            print('  ],')
        elif title == 'EXP':
            kvar.read(data_count)
        elif title in ('ISP', 'ISP0', 'ISP1'):
            kvar.read(data_count)
        elif title == 'FRAME_ISP':
            kvar.read(data_count)
        elif title == 'INFO':
            info, = unpack('%ss' % data_count ,kvar.read(data_count))
            print('  "info": "%s",' %bytes2str(info))
        elif title == 'PTS_UNIT':
            pts_unit, = unpack('<I',kvar.read(4))
            print('  "pts_unit": %s,'% pts_unit)
            pts_unit = 1000000 if pts_unit == 1 else 1000
        elif title == 'PTS':
            print('  "pts": [')
            for idx in range(int(data_count)):
                pts, = unpack('<Q', kvar.read(8))
                if idx == 0:
                    pts_start = pts
                print('%s%s' % (pts/pts_unit, ',' if idx<int(data_count)-1 else ''))
            print('  ],')

        else:
            print('!!!! UNKOWN SECTION : ', title, data_type, data_count)
            exit(1)
    print(' "software": "kvar2json" }')
