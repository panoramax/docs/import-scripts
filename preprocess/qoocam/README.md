# Qoocam pre-process scripts

## qoocam2panoramax.py

Input:
- Qoocam time-lapse video MP4 file
- Qoocam sensors binary file (.kvar)

Output:
- serie of JPEG files, with EXIF tags, deduplicated

use '--help' option to get help about parameters.

## kvar2json.py

Convert a Qoocam sensors binary file to JSON
