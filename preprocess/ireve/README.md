# ireve2panoramax.py

Ce script permet de préparer des séries de photos issues du logiciel Ireve afin de les verser dans Panoramax.

L'heure étant absente des métadonnées d'origine, celle-ci est recalculée artificiellement en partant de midi, en ajoutant les secondes recalculée à partir de la vitesse et de la distance, et en sautant à la minute suivante en cas de rupture de plus de 10m entre deux photos.

Seules les photos de la caméra gauche sont traitées, Panoramax ne gérant pas de mode stéréoscopique.

cq - 2025-02-10
