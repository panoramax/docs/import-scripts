#! /usr/bin/python

import sys, csv, datetime
from math import radians, cos, sin, asin, sqrt

try:
    import pyexiv2

except:
    print("Missing python modules: pyexiv2 is required", file=sys.stderr)
    print("  pip install pyexiv2", file=sys.stderr)


def haversine(lat1, lon1, lat2, lon2):

      R = 6372800 # Earth radius in meters

      dLat = radians(lat2 - lat1)
      dLon = radians(lon2 - lon1)
      lat1 = radians(lat1)
      lat2 = radians(lat2)

      a = sin(dLat/2)**2 + cos(lat1)*cos(lat2)*sin(dLon/2)**2
      c = 2*asin(sqrt(a))

      return R * c


dir = sys.argv[1]

# chargement des positions GPS
with open(dir+'GPS/Position.csv') as gpsfile:
    gps = list(csv.reader(gpsfile,delimiter=';'))

# chargement de la liste des images
with open(dir+'Imaging.Front/ImagePair.csv') as imgfile:
    imglist = list(csv.reader(imgfile,delimiter=';'))

cur_gps = 1
last_lon = None
last_lat = None
last_time = None

for i in range(len(imglist)):
    img = imglist[i+1]
    pk = float(img[0]) # point kilométrique de l'image
    if pk<float(gps[cur_gps][0]):
        continue # on saute si pas de position GPS couvrant ce pk
    try:
        while pk>float(gps[cur_gps+1][0]):
            cur_gps = cur_gps+1 # passage aux couple de position GPS suivant
    except:
        exit()

    # interpolation entre pk image et GPS
    lat = float(gps[cur_gps][7])
    lon = float(gps[cur_gps][8])
    ele = float(gps[cur_gps][9])
    gpk = float(gps[cur_gps][0])
    dlat = float(gps[cur_gps+1][7]) - float(gps[cur_gps][7])
    dlon = float(gps[cur_gps+1][8]) - float(gps[cur_gps][8])
    dele = float(gps[cur_gps+1][9]) - float(gps[cur_gps][9])
    dist = float(gps[cur_gps+1][0]) - float(gps[cur_gps][0])
    img_lon = round(lon + dlon / dist * (pk-gpk),7)
    img_lat = round(lat + dlat / dist * (pk-gpk),7)
    img_ele = round(ele + dele / dist * (pk-gpk),1)
    img_speed = float(img[9])

    if last_lon and last_lat:
        meters = haversine(last_lat, last_lon, img_lat, img_lon)
        seconds = meters / img_speed
        img_time = last_time + seconds
        if meters > 10:
            img_time = img_time + 60
    else:
        img_time = 0

    path = dir+'/Imaging.Front/ImagePair.Left/'+img[7]
    try:
        jpg = pyexiv2.Image(path)
    except:
        print('ERROR',path)
        continue

    
    longitude = '%s/1000000 0/1 0/1' % int(abs(img_lon)*1000000)
    longituderef = 'E' if img_lon >=0 else 'W'
    latitude = '%s/1000000 0/1 0/1' % int(abs(img_lat)*1000000)
    latituderef = 'N' if img_lat >=0 else 'S'
    speed = '%s/10' % int(img_speed*10*3.6) # m/s -> km/h
    subsec = '00'+str(int(round(img_time % 1,3) * 1000))
    time = datetime.datetime.fromtimestamp(int(img_time)+43200).isoformat()[-8:]

    tags = jpg.read_exif()
    exif_datetime = tags['Exif.Photo.DateTimeOriginal']
    jpg.modify_exif({
        'Exif.GPSInfo.GPSLongitude':            longitude,
        'Exif.GPSInfo.GPSLongitudeRef':         longituderef,
        'Exif.GPSInfo.GPSLatitude':             latitude,
        'Exif.GPSInfo.GPSLatitudeRef':          latituderef,
        'Exif.GPSInfo.GPSSpeed':                speed,
        'Exif.Image.Artist':                    'DIR-MC',
        'Exif.Photo.DateTimeOriginal':          exif_datetime.replace(exif_datetime[-8:],time),
        'Exif.Photo.SubSecTimeOriginal':        subsec[-3:],
        'Exif.Photo.DateTimeDigitized':         None,  
        'Exif.Photo.SubSecTimeDigitized':       None,
        'Exif.GPSInfo.GPSHPositioningError':    0.5,
        })
    jpg.modify_xmp({'Xmp.GPano.ProjectionType': None})

    last_lon = img_lon
    last_lat = img_lat
    last_time = img_time

    jpg.close()
