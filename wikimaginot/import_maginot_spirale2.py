#!/usr/bin/env python3

# cSpell:ignore dotenv geovisio

import glob
import os
import sys
from dotenv import load_dotenv
from pathlib import Path

#######################################
# Generate the upload bash for geovisio
#######################################

if __name__ == "__main__":

	# read the dotenv
	load_dotenv('.env')
	WM_STORAGE = os.environ.get('WM_STORAGE')
	WM_URL = os.environ.get('WM_URL')
	WM_TOKEN = os.environ.get('WM_TOKEN')

	pwd = os.getcwd()
	print(f"# pwd         : {pwd}")
	p_local_files = Path(pwd, WM_STORAGE)
	print(f"# local files : {p_local_files}\n")


	# read the folders and do the uploads :
	list_of_seq = []
	list_of_seq.extend(glob.glob(os.path.join(p_local_files, f"*")))
	if len(list_of_seq) < 1:
		print(f"error, no subfolder in {p_local_files} !")
		sys.exit(1)

	list_of_seq.sort()
	for num_seq in list_of_seq:
		print(f"geovisio upload --token {WM_TOKEN} --title \"$(cat {num_seq}/titre.txt)\" --sort-method filename-asc --api-url {WM_URL} {num_seq}")
