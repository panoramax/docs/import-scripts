#!/usr/bin/env python3

# cSpell:ignore dotenv geovisio wikimaginot spiderfy Exif KHTML

import argparse
import csv
import glob
import math
import os
import requests
import sys
from dotenv import load_dotenv
from pathlib import Path

# ###########################################
licences = {"IGN": ['Domaine public','CC-O'],
			"OSM": ['CC-BY','CC-BY-SA']}
# ###########################################

def _generateSpiralPoints(count:int, x:float, y:float) -> list:
	# print(f"_generateSpiralPoints() : {count} {x} {y} ")
	_2PI = math.pi  * 2
	# _spiralLengthStart = 0.000055
	_spiralLengthStart = 0.0000275
	_spiralFootSeparation = 0.00014 #related to size of spiral (experiment!)
	_spiralLengthFactor = 0.000025
	# _spiralLengthFactor = 0.0000125

	spiderfyDistanceMultiplier = 1
	legLength = spiderfyDistanceMultiplier * _spiralLengthStart
	separation = spiderfyDistanceMultiplier * _spiralFootSeparation
	lengthFactor = spiderfyDistanceMultiplier * _spiralLengthFactor * _2PI
	angle = 0
	res = []

	# Higher index, closer position to cluster center.
	for i in range(count, 0, -1):
		# Skip the first position, so that we are already farther from center and we avoid
		# being under the default cluster icon (especially important for Circle Markers).
		if (i == count):
			res.append( [x, y])
		else:
			res.append( [round(x + legLength * math.cos(angle), 6), round(y + legLength * math.sin(angle), 6)] )
		angle += separation / legLength + i * 0.0005
		legLength += lengthFactor / angle
	return res

def _make_date(date:str, str_url:str) -> str:
	ret_time = "T00:00:00"
	ret_date = "1900-01-01" + ret_time
	if date == "":
		if str_url.find('/'):
			date_folder = str_url.split('/')[-3]
			if date_folder.startswith('20') and len(date_folder) == 4:
				ret_date = date_folder + "-01-01" + ret_time
			elif date_folder.startswith('20') and len(date_folder) == 7:
				ret_date = date_folder + "-01" + ret_time
			elif date_folder.startswith('20') and len(date_folder) == 10:
				ret_date = date_folder + ret_time
	elif date.find('/'):
		ret_date = date.split('/')[2] + "-" + date.split('/')[1] + "-" + date.split('/')[0] + ret_time

	return ret_date

def read_csv_and_download_images(p_local_files: Path, p_csv_in: Path) -> None:
	################
	# read csv, download photos, create titre.txt + _geovisio.csv :
	################
	# Num_photo;Lat;Lon;Titre;Sujet;Auteur;Licence;Date_photo;Url_photo;Url_site
	# 93407;48.932294;7.951462;"H O F F E N - (Abri)";;"wikimaginot - Gregory Fuchs";CC-BY-SA;07/06/2019;https://files-wikimaginot.eu/_documents/2021-03/_wiki_photos_redim/63-1616490496.jpg;https://wikimaginot.eu/V70_construction_detail.php?id=10961
	################
	with p_csv_in.open('r', newline='') as file_in:
		o_reader = csv.DictReader(file_in, delimiter=';', quotechar='"')
		for row in o_reader:
			# 1.A read the row and store items.
			# photo number
			s_num = row['Num_photo']
			# check licence
			if row['Licence'] not in licences.get(s_licence):
				print(f"error, bad Licence for {s_num} : {row['Licence']} not in '{licences.get(s_licence)}' !")
				continue
			# check id
			if "?id=" not in row['Url_site']:
				print(f"error, no id in Url_site for {s_num} !")
				continue
			s_id_seq = row['Url_site'][row['Url_site'].find("?id=")+4:]
			# check lat,lon,titre
			if row['Lat'] == "":
				print(f"error, no Lat for {s_num} !")
				continue
			if row['Lon'] == "":
				print(f"error, no Lon for {s_num} !")
				continue
			if row['Titre'] == "":
				print(f"error, no Titre for {s_num} !")
				continue
			# photo
			if row['Url_photo'] == "":
				print(f"error, no Url_photo for {s_num} !")
				continue
			s_url = row['Url_photo']
			if not s_url.find('/'):
				print(f"error, no photo name in Url_photo for {s_num} !")
				continue
			s_name_photo = s_url.split('/')[-1]
			# # check no png
			if s_name_photo.lower().endswith(".png"):
				print(f"error, PNG file for {s_num} !")
				continue
			# local photo
			p_local_folder = Path(p_local_files, s_id_seq)
			p_local_photo = Path(p_local_folder, s_name_photo)
			# date
			s_raw_date = row['Date_photo']
			s_date = _make_date(s_raw_date, s_url)
			# auteur
			s_author : str = row['Auteur']
			if s_author.startswith("wikimaginot - "):
				s_author = row['Auteur'][14:]
			# # check no Google
			if "google" in s_author.lower():
				print(f"error, Google photo for {s_num} !")
				if p_local_photo.exists():
					os.remove(p_local_photo)
				continue

			# 1.B download photo
			if not p_local_photo.exists():
				print(f"downloading {s_url} ...")
				headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'}
				with requests.get(url=s_url, headers=headers, verify=True, allow_redirects=True, stream=True) as response_photo:
					if response_photo.status_code == 200:
						# create folder
						if not p_local_folder.exists():
							p_local_folder.mkdir(parents = True)
						# create photo
						with p_local_photo.open('wb') as photo_file:
							photo_file.write(response_photo.content)
					else:
						print(f"Http error for {s_id_seq}, status code = {response_photo.status_code}")
						continue
			# else:
				# print(f"info, {p_local_photo} already exists")

			# 1.C create titre.txt if not exists
			p_titre = Path(p_local_folder, 'titre.txt')
			if not p_titre.exists():
				with p_titre.open('w', encoding='utf-8') as titre_file:
					titre_file.write(row['Titre'])

			# 1.D create _geovisio.csv.tmp
			p_tmp = Path(p_local_folder, '_geovisio.csv.tmp')
			b_write_first_ligne = True
			if p_tmp.exists():
				b_write_first_ligne = False
			with p_tmp.open('a', newline='') as tmp_file:
				o_writer = csv.writer(tmp_file, delimiter=';', quotechar='"', escapechar='\\', quoting=csv.QUOTE_ALL)
				if b_write_first_ligne:
					o_writer.writerow(["file","lat","lon","capture_time","Exif.Image.Artist","Xmp.xmp.BaseURL"])
				o_writer.writerow([s_name_photo, row['Lat'], row['Lon'], s_date, s_author, row['Url_site']])

def modify_xy(p_local_files: Path) -> None:
	################
	# read geovisio.csv in folders, modify x,y :
	################
	list_of_seq = []
	list_of_seq.extend(glob.glob(os.path.join(p_local_files, f"*")))
	if len(list_of_seq) < 1:
		print(f"error, no subfolder in {p_local_files} !")
		sys.exit(1)

	list_of_seq.sort()
	for num_seq in list_of_seq:
		p_csv_tmp = Path(num_seq)/"_geovisio.csv.tmp"
		with p_csv_tmp.open('r', newline='') as tmp_file:
			# 1. read the csv
			# "file","lat","lon","capture_time","Exif.Image.Artist","Xmp.xmp.BaseURL"
			o_reader = csv.DictReader(tmp_file, delimiter=';', quotechar='"')
			d_images = {}
			lat:float = 0.0
			lon:float = 0.0
			for row in o_reader:
				if lat == 0.0 and lon == 0.0:
					lat = float(row['lat'])
					lon = float(row['lon'])
				else:
					if float(row['lat']) != lat or float(row['lon']) != lon:
						print(f"error, lat/lon are different for {num_seq} !!!")
				d_images[row['file']] = [float(row['lon']), float(row['lat']), row['capture_time'], row['Exif.Image.Artist'], row['Xmp.xmp.BaseURL'], ]
			nb:int = o_reader.line_num # pour le nombre de lignes
			# print(f"Nb de lignes pour {num_seq} : {nb}")
			# print(f">>> {d_images}")

			# 2. generate new x,y
			liste_lon_lat = _generateSpiralPoints((nb - 1), lon, lat)
			print(f">>>> {liste_lon_lat}")
			dict_out = {}
			i = 0
			for k in sorted(d_images.keys()):
				dict_out[k] = [liste_lon_lat[i][0], liste_lon_lat[i][1], d_images[k][2], d_images[k][3], d_images[k][4]]
				i += 1
			# print(f">>> {dict_out}")

			# 3. generate the new csv
			p_file_out = Path(num_seq)/"_geovisio.csv"
			with p_file_out.open('w', newline='') as new_csv:
				fieldnames = ["file","lat","lon","capture_time","Exif.Image.Artist","Xmp.xmp.BaseURL"]
				writer = csv.DictWriter(new_csv, fieldnames=fieldnames, delimiter=';', quotechar='"', escapechar='\\', quoting=csv.QUOTE_ALL)
				writer.writeheader()
				for k in sorted(dict_out.keys()):
					writer.writerow({'file': k, 'lat': dict_out[k][1], 'lon': dict_out[k][0], 'capture_time': dict_out[k][2], 'Exif.Image.Artist': dict_out[k][3], 'Xmp.xmp.BaseURL': dict_out[k][4]})


if __name__ == "__main__":
	#####################################
	# Generate local folders for geovisio
	#####################################

	# read the dotenv
	load_dotenv('.env')
	WM_STORAGE = os.environ.get('WM_STORAGE')

	pwd = os.getcwd()
	print(f"pwd         : {pwd}")
	p_local_files = Path(pwd, WM_STORAGE)
	print(f"local files : {p_local_files}\n")

	# parse args
	parser = argparse.ArgumentParser()
	parser.add_argument("-f", "--file", help="Cvs file from WikiMaginot.eu (mandatory)", type=str, required=True)
	parser.add_argument("-l", "--licence", help="Type of licence : IGN or OSM (mandatory)", type=str, required=True)
	parser.add_argument("-p", "--part", help="Part : 1 or 2. Only part '1' with '-p 1' (optional)", type=str, required=False)
	args = parser.parse_args()

	p_csv_in = Path(pwd, args.file)
	s_licence = args.licence
	print(f"csv file in : {p_csv_in}")
	print(f"licence     : {s_licence}\n")
	# check licence
	if s_licence not in licences.keys():
		print(f"error, licence {s_licence} not in '{licences.keys()}' !")
		sys.exit(1)

	# part 1
	if args.part != "2":
		read_csv_and_download_images(p_local_files, p_csv_in)

	# part 2
	if args.part != "1":
		modify_xy(p_local_files)
