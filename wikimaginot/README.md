# Import WikiMaginot

Ce dépôt fournit les outils pour uploader les photos (et les métadonnées) de WikiMaginot vers les instances IGN et OSM de Panoramax.

## Fichier de départ (cvs exporté de wikimaginot.eu)

Le fichier csv comprend 10 colonnes.
Par exemple :

```
Num_photo;Lat;Lon;Titre;Sujet;Auteur;Licence;Date_photo;Url_photo;Url_site
10828;49.021719;7.252502;"R602 (Chambre de coupure) ";;"wikimaginot - Christatus";CC-BY;;https://files-wikimaginot.eu/_documents/2011/_wiki_photos_redim/30-1321810396.JPG;https://wikimaginot.eu/V70_construction_detail.php?id=11631
10827;49.021719;7.252502;"R602 (Chambre de coupure) ";;"wikimaginot - Christatus";CC-BY;05/04/2009;https://files-wikimaginot.eu/_documents/2021-06/_wiki_photos_redim/63-1623253016.jpg;https://wikimaginot.eu/V70_construction_detail.php?id=11631
11091;49.054370;7.353364;"B - (Chambre de coupure)";;"wikimaginot - Christatus";CC-BY;18/02/2011;https://files-wikimaginot.eu/_documents/2021-07/_wiki_photos_redim/63-1626528361.jpg;https://wikimaginot.eu/V70_construction_detail.php?id=11902
11090;49.054370;7.353364;"B - (Chambre de coupure)";;"wikimaginot - Christatus";CC-BY;18/02/2011;https://files-wikimaginot.eu/_documents/2021-07/_wiki_photos_redim/63-1626528390.jpg;https://wikimaginot.eu/V70_construction_detail.php?id=11902
29796;50.371584;3.601278;"B461 - RESERVOIR D'ONNAING - (Blockhaus pour canon)";;"wikimaginot - Google Streetview";CC-O;02/04/2015;https://files-wikimaginot.eu/_documents/2021-10/_wiki_photos_redim/63-1633348951.jpg;https://wikimaginot.eu/V70_construction_detail.php?id=18934
(...)
```

## Import des photos dans les instances de Panoramax

Les photos ayant pour licence "Domaine public" et CC-O seront importées dans l'instance IGN.
Celles de licence CC-BY et CC-BY-SA seront importées dans l'instance OSM.

Le premier script (**import_maginot_spirale.py**) permet de downloader les photos en local, et de créer les fichiers de métadonnées.

Les photos seront regroupées par identifiant (id) de séquence **"instance/id de Url_site"** comme ceci :
```
/racine/OSM/11631/30-1321810396.JPG
/racine/OSM/11631/63-1623253016.jpg
/racine/OSM/11902/63-1626528361.jpg
(...)
```

Dans chaque sous-dossier "id", en plus des photos, on aura les 2 fichiers :

- **titre.txt** : contient le champ "Titre"

- **_geovisio.csv** : un fichier de métadonnées compatible avec la CLI ([format attendu](https://gitlab.com/geovisio/cli/-/blob/main/README.md?ref_type=heads#external-metadata)). Il contient les champs suivants :

```
file;lat;lon;capture_time;Exif.Image.Artist;Xmp.xmp.BaseURL
30-1321810396.JPG;49.021719;7.252502;"2011-01-01T00:00:00";"Christatus";https://wikimaginot.eu/V70_construction_detail.php?id=11631
63-1623253016.jpg;49.021719;7.252502;"2009-04-05T00:00:00";"Christatus";https://wikimaginot.eu/V70_construction_detail.php?id=11631
(...)
```

Le deuxième script (**import_maginot_spirale2.py**) permet de générer le bash d'upload vers une instance de Panoramax.

Il faut au préalable renseigner l'url de l'instance ainsi que son token dans un fichier .env.

## Volumétrie

CC-BY          : 7074 photos

CC-BY-SA       : 6286 photos

CC-O           : 1786 photos

Domaine public : 1319 photos
